from pyramid.config import Configurator


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    config = Configurator(settings=settings)

    config.include('pyramid_jinja2')

    # Sass Setup
    config.include("pyramid_scss")
    config.add_route('css', '/css/{css_path:.*}.css')
    config.add_view(route_name='css', view='pyramid_scss.controller.get_scss', renderer='scss', request_method='GET')

    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('home', '/')
    config.add_route('tutorial', '/tutorial')
    config.add_route('option', '/ticker/{ticker}')
    config.scan()
    return config.make_wsgi_app()
