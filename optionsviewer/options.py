"""
Wrapper for the Tradier API. 
"""
from enum import Enum
import requests

class Endpoints(Enum):
    ROOT = 'https://sandbox.tradier.com'

    # GET Endpoints
    SEARCH = '/v1/markets/search'
    SYMBOL = '/v1/markets/lookup'
    QUOTE = '/v1/markets/quotes'
    PRICES = '/v1/markets/history'
    STRIKES = '/v1/markets/options/strikes'
    EXPIRATIONS = '/v1/markets/options/expirations'
    CHAINS = '/v1/markets/options/chains'

    # POST Endpoints
    STREAM = '/v1/markets/events/session'

class Options():
    """
    Object that is called in view.py. Retrieves data from the various API endpoints.
    """
    access_key = 'WDkeW2VBuWQxPmNK8ZAGOB0DZPDu'

    def __init__(self, *args, **kwargs):
        self.session = requests.Session()
        self.session.headers = {
            'Accept' : 'application/json',
            'Authorization' : 'Bearer {}'.format(self.access_key)
        }

    #########
    #   Search Methods
    #########

    def search_company(self, query):
        res = self.session.get(Endpoints.SEARCH, params={
            'q' : query
        })
        res.raise_for_status()
        return res.json()['securities']['security']

    def search_symbol(self, query):
        res = self.session.get(Endpoints.SYMBOL, params={
            'q' : query
        })
        res.raise_for_status()
        return res.json()['securities']['security']

    #########
    #   Stock Methods
    #########

    def get_quote(self, symbols):
        res = self.session.get(Endpoints.EXPIRATIONS, params={
            'symbols' : symbols
        })
        res.raise_for_status()
        return res.json()['quotes']['quote']

    #########
    #   Options Methods
    #########

    def get_expirations(self, symbol):
        res = self.session.get(Endpoints.EXPIRATIONS, params={
            'symbol' : symbol
        })
        res.raise_for_status()
        return res.json()['expirations']['date']

    def get_chains(self, symbol, expiration):
        res = self.session.get(Endpoints.EXPIRATIONS, params={
            'symbol' : symbol,
            'expiration' : expiration
        })
        res.raise_for_status()
        return res.json()['options']['option']

    def get_strikes(self, symbol, expiration):
        res = self.session.get(Endpoints.EXPIRATIONS, params={
            'symbol' : symbol,
            'expiration' : expiration
        })
        res.raise_for_status()
        return res.json()['strikes']['strike']

    #########
    #   Streaming Methods
    #########

    def get_session_id(self):
        res = self.session.post(Endpoints.STREAM)
        res.raise_for_status()
        return res.json()['stream']['sessionid']