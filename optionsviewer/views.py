from pyramid.view import view_config
# from options import Options
import json

@view_config(route_name='home', renderer='templates/home.jinja2')
def home(request):
    return {'project': 'optionsviewer'}

@view_config(route_name='tutorial', renderer='templates/tutorial.jinja2')
def tutorial(request):
    with open('optionsviewer/assets/text/vocabulary.json') as f:
        vocab = json.load(f)
    with open('optionsviewer/assets/text/contract-types.json') as f:
        optiontypes = json.load(f)
    with open('optionsviewer/assets/text/thegreeks.json') as f:
        greeks = json.load(f)
    with open('optionsviewer/assets/text/spreads.json') as f:
        spreads = json.load(f)
    return {
        'vocabulary': vocab, 
        'optiontypes': optiontypes,
        'greeks': greeks,
        'spreads': spreads
    }

@view_config(route_name='option', renderer='templates/option.jinja2')
def option(request):
    return {
        'security':{
            'ticker': '%(ticker)s' % request.matchdict,
            'change': 1,
            'percent': 10,
            'price': 100,
            'up':True,
            'quote':{
                'Open': 99,
                'High':101,
                'Low':99,
                'Volume':10000,
                }
            }
        }