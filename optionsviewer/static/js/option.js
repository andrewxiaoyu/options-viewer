/**
 * Parameters
 */
const priceRangeMultiplier = .2;

$(document).ready(()=>{
    spreadGraph();
});

/**
 * Spread Graph
 */

 // Returns vertical line at the specified price
function getCurrentPriceTrace(price){
    return {
        type: 'line',
        yref: 'paper',
        x0: price,
        y0: 0,
        x1: price,
        y1: 1,
        line:{
            color: '#FFFD98', // YELLOW
            width: 3,
            dash:'dot'
        }
    }
}

// Finds the range by looking at a 10% change in the price 
function getXRange(currentPrice){
    let change = currentPrice * priceRangeMultiplier;
    let low = currentPrice - priceRangeMultiplier;
    let high = currentPrice + priceRangeMultiplier;
    return [low, high];
}

// Finds the range by looking at the max profit or loss
function getYRange(maxProfit, minProfit){
    let r = Math.max( Math.abs(maxProfit), Math.abs(minProfit) );
    return [-r, r];
}

function spreadGraph(){
    var trace = {
        x: [1, 2, 3, 4],
        y: [-10, 10, 10, -10],
        fill: 'tozeroy',
        type: 'scatter',
        name: 'Spread',
        line:{
            color: '#99D5C9' //Header
        }
    };
    var shape = getCurrentPriceTrace(2.5);
    let customLayout = plotLayout;
    customLayout['xaxis']['title'] = 'Security Price';
    customLayout['xaxis']['range'] = getXRange(2.5);
    
    customLayout['yaxis']['title'] = 'Profit';
    customLayout['yaxis']['range'] = getYRange(10,-10);
    customLayout['shapes'] = [shape];

    var data = [trace];

    Plotly.newPlot('graph', data, plotLayout, plotConfiguration);
}

/**
 * Altering Options
 */
function addOption(){

}

/**
 * Default Plot Configuration
 */
const plotLayout = {
    scrollZoom: true,
    paper_bgcolor: 'rgba(0,0,0,0)',
    plot_bgcolor: 'rgba(0,0,0,0)',
    // hovermode: 'closest',
    showlegend: false,
    margin: {
        l: 50,
        r: 0,
        b: 50,
        t: 0,
        pad: 5
    },
    title: false,
    xaxis: {
        title: 'CUSTOM',
        zeroline: false,
        titlefont: {
            family: 'Lato, sans-serif',
            color: 'white'
        },
        tickfont: {
            family: 'Lato, sans-serif',
            color: 'white'
        },
        range: [0, 1] // CUSTOMIZE
    },
    yaxis: {
        title: 'CUSTOM',
        zeroline: false,
        titlefont: {
            family: 'Lato, sans-serif',
            color: 'white'
        },
        tickfont: {
            family: 'Lato, sans-serif',
            color: 'white'
        },
        range: [0, 1] // CUSTOMIZE
    }
}
const plotConfiguration = {
    scrollZoom: true,
    // displayModeBar: false,
    displaylogo: false,
    modeBarButtonsToRemove: ['sendDataToCloud','select2d','lasso2d','autoScale2d','hoverClosestCartesian','hoverCompareCartesian']
}