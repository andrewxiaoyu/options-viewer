$(document).ready(()=>{
    $("#background-design").interactive_bg({
        strength: 25,
        scale: 1.000,
        animationSpeed: "100ms",
        contain: true,
        wrapContent: false
    });
    
});

$(window).resize(function() {
    $("#background-design > .ibg-bg").css({
      width: $(window).outerWidth(),
      height: $(window).outerHeight()
    })
 })